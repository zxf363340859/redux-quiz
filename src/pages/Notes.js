import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {setNote} from "../action/fetchAction";
import {connect} from "react-redux";

class Notes extends Component{

    constructor(props) {
        super(props);
        fetch("http://localhost:8080/api/posts")
            .then(response => response.json())
            .then((result) => {
                this.props.loadNote(result);
            });
    }
    // 这里有个问题，为什么不可以直接从redux中获取呢

    render() {
        return (
            <div>
                <p>testadssad</p>
            </div>
        );
    }
}


const mapDispatchToProps = (dispatch) => bindActionCreators({
    loadNote: setNote
}, dispatch);

const mapStateToProps = (state) => ({
    noteReducer: state.noteReducer
});

export default connect(mapStateToProps, mapDispatchToProps)(Notes);

