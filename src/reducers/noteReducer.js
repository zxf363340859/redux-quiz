const initState = {
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'SET_NOTE':
            return {
                notes:action.notes
            };
        default :
            return state;
    }
}
