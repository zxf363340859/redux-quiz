import React, {Component} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import './App.less';
import Home from "./Home/Home";
import Note from "./components/Note";
import CreateNote from './components/CreateNote'
import Notes from "./pages/Notes";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route path='/notes/create' component={CreateNote}/>
                <Route path='/notes/:id' component={Note}/>
                <Route path='/test' component={Notes}/>
            </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
