export const setNote = (result) => (dispatch) => {
    dispatch({
        type: 'SET_NOTE',
        notes: result
    });
};
