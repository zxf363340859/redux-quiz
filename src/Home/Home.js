import React from 'react';
import {bindActionCreators} from "redux";
import {setNote} from "../action/fetchAction";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class Home extends React.Component {

    constructor(props) {
        super(props);
        fetch("http://localhost:8080/api/posts")
            .then(response => response.json())
            .then((result) => {
                this.props.loadNote(result);
            });
    }




    render() {
        return (
            <div>
                <h1>NOTE</h1>
                <nav>
                    <ul>
                        {
                            this.props.noteReducer.notes !== undefined ?
                                this.props.noteReducer.notes.map((item, index) =>
                                   <li key={index}><Link to={`/notes/${item.id}`}>{item.title}</Link></li>
                                ) :
                                null
                        }
                    </ul>
                </nav>
                <Link to={'/notes/create'}>创建笔记</Link>
            </div>
        );
    }

}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    loadNote: setNote
}, dispatch);

const mapStateToProps = (state) => ({
    noteReducer: state.noteReducer
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
