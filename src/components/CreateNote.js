import React from 'react';
import {Link} from "react-router-dom";

class CreateNote extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: ''
        };
        this.handleTitle = this.handleTitle.bind(this);
        this.handleBody = this.handleBody.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    handleTitle(event) {
        this.setState({
            title: event.target.value
        });
    }

    handleBody(event) {
        this.setState({
            description: event.target.value
        });
    }

    handleClick() {
        const post = {
            title: this.state.title,
            description: this.state.description
        };
        fetch("http://localhost:8080/api/posts", {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(post)
        })
            .then(this.handleBack);
    }

    handleBack(){
        this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <h1>NOTE</h1>
                <h1>create note</h1>
                <hr />
                标题
                　<input onChange={this.handleTitle} />
                内容
                <input onChange={this.handleBody} type={'textarea'}/>
                <button onClick={this.handleClick}>提交</button>
                <Link to={'/'}>cancel</Link>
            </div>
        );
    }

}


export default CreateNote;

