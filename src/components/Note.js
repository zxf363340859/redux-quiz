import React from 'react';
import {bindActionCreators} from "redux";
import {setNote} from "../action/fetchAction";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class Note extends React.Component {

    constructor(props) {
        super(props);

        fetch("http://localhost:8080/api/posts")
            .then(response => response.json())
            .then((result) => {
                this.props.loadNote(result);
            });
        this.handleBack = this.handleBack.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleBack(){
        this.props.history.push('/');
    }

    handleDelete(){
        fetch(`http://localhost:8080/api/posts/${this.props.match.params.id}`, {
            method: 'DELETE'
        })
            .then(this.handleBack);
    }

    render() {
        return (
            <div>
                <section>
                    {
                        this.props.noteReducer.notes !== undefined ?
                            this.props.noteReducer.notes.map((item, index) =>
                                <li key={index}><Link to={`/notes/${item.id}`}>{item.title}</Link></li>
                            ) :
                            null
                    }
                </section>
                <section>
                    {
                        this.props.noteReducer.notes !== undefined ?
                            this.props.noteReducer.notes.map((item) => {
                                if (item.id == this.props.match.params.id + "") {
                                    return (
                                        <div>
                                            <h1>{item.title}</h1>
                                            <p>{item.description}</p>
                                        </div>
                                    );
                                }
                            }) :
                            null
                    }
                </section>
                <button onClick={this.handleDelete}>删除</button>
                <button onClick={this.handleBack}>返回</button>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    loadNote: setNote
}, dispatch);

const mapStateToProps = (state) => ({
    noteReducer: state.noteReducer
});

export default connect(mapStateToProps, mapDispatchToProps)(Note);

