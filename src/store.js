import {applyMiddleware, compose, createStore} from "redux";
import reducers from "./reducers";
import thunk from "redux-thunk";

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = (initialState) => createStore(
  reducers,
    initialState,
  composeEnhancer(applyMiddleware(thunk)),
);

export default store ({});
